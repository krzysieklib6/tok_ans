module mailFx {
    requires javafx.controls;
    requires javafx.graphics;
    requires javafx.fxml;
    requires java.mail;
    requires org.postgresql.jdbc;
    requires java.sql;

    exports app.main;
    exports app.model;
    exports app.connection;
    opens  app.controller to javafx.fxml;
    opens  app.model to javafx.fxml;

}