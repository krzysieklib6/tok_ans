package app.controller;

import app.connection.Connect;
import app.main.Main;
import app.model.Student;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.sql.SQLException;

public class TableViewEmailPaneController {


//    @FXML
//    private TableView<Student> table;
//    @FXML
//    private VBox vbox;
    @FXML
    private AnchorPane anchorPane;

    @FXML
    private VBox vBox;

    @FXML
    private TableColumn<Student, String> emailColumn;

    @FXML
    private TableColumn<Student, String> lastNameColumn;

    @FXML
    private TableColumn<Student, String> nameColumn;

    @FXML
    private TableView<Student> table;





    public void initialize() throws SQLException {
        Connect connect = new Connect();
        Button buttonBack = new Button();
        buttonBack.setText("Switch back");
        buttonBack.setLayoutX(500);
        buttonBack.setLayoutY(325);
        nameColumn = new TableColumn<>("Name");
        lastNameColumn = new TableColumn<>("Last Name");
        emailColumn = new TableColumn<>("Email");

        emailColumn.setCellValueFactory(new PropertyValueFactory<Student,String>("email"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<Student,String>("lastName"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<Student,String>("name"));
        emailColumn.setMinWidth(200);
        table = new TableView<>();
        table.setItems(connect.getStudent());
        table.getColumns().addAll(nameColumn,lastNameColumn,emailColumn);

        final VBox vbox = new VBox();
        vbox.setSpacing(5);
        vbox.setPadding(new Insets(10, 10, 10, 10));
        vbox.getChildren().addAll(table);
        anchorPane.getChildren().addAll(vbox,buttonBack);

        buttonBack.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                try {
                    changeActivity();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void changeActivity() throws IOException {
        Main main = new Main();
        main.setRoot("/sendEmailPane.fxml");
    }


}
