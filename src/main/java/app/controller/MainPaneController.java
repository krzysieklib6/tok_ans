package app.controller;

import app.main.Main;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.io.IOException;

public class MainPaneController {

    @FXML
    private Button buttonLogin;

    @FXML
    private TextField emailTextField;

    @FXML
    private TextField loginTextField;

    @FXML
    private PasswordField passwordTextField;

    @FXML
    private void clickToChangeActivity() throws IOException {
        Main main = new Main();
      main.setRoot("/sendEmailPane.fxml");
    }

    public void initialize() {

            buttonLogin.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    if (!loginTextField.getText().isEmpty() && loginTextField.getText().equals("admin") &&
                            !passwordTextField.getText().isEmpty() && passwordTextField.getText().equals("admin") &&
                            !emailTextField.getText().isEmpty() && emailTextField.getText().equals("admin")) {
                        try {
                            clickToChangeActivity();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

    }





}