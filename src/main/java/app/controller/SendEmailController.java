package app.controller;

import app.connection.Connect;
import app.main.Main;
import app.main.SendEmail;
import app.model.Student;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.io.IOException;

public class SendEmailController {

    @FXML
    private TextField addresatTextField;

    @FXML
    private Button buttonSendEmail;

    @FXML
    private TextArea emailTextArea;

    @FXML
    private TextField topicTextField;

    @FXML
    private Button buttonChangeActivity;

    @FXML
    void clickToChangeActivity() throws IOException {
        Main main = new Main();
        main.setRoot("/tableViewEmailPane.fxml");
    }

    public void initialize() {

        buttonChangeActivity.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                try {
                    clickToChangeActivity();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        buttonSendEmail.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                if (!addresatTextField.getText().isEmpty() && !emailTextArea.getText().isEmpty() && !topicTextField.getText().isEmpty()) {
                    SendEmail sendEmail = new SendEmail(addresatTextField.getText(),emailTextArea.getText(),topicTextField.getText());
                }
            }
        });

    }



}
