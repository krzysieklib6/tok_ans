package app.connection;

import app.model.Student;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;
public class Connect {

    private String driver = "org.postgresql.Driver";

    private String host = "195.150.230.210";

    private String port = "5436"; //wymagane kiedy nie jest domyślny dla bazy

    private String dbname = "2022_ksiazek_krzysztof";
    private String user = "2022_ksiazek_krzysztof";

    private String url = "jdbc:postgresql://" + host + ":" + port + "/" + dbname;
    private String password = "34301";

    private Connection connection;

    public Connect ()  {}

    public ObservableList<Student> getStudent() throws SQLException {
        ObservableList<Student> students = FXCollections.observableArrayList();
        connection = makeConnection();
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(
                "SELECT " +
                        "dzs.imie,dzs.nazwisko,dzo.email " +
                        "FROM " +
                        "dziekanat.studenci dzs, dziekanat.osobiste dzo " +
                        "WHERE " +
                        "dzs.nr_albumu = dzo.nr_albumu " +
                        "AND " +
                        "email is not NULL;");

        ResultSetMetaData rsmd = rs.getMetaData();
        while (rs.next()) {
           System.out.print(rs.getString(1) +"|" + rs.getString(2) +"|"+ rs.getString(3) + "\n");
            students.add(new Student(rs.getString(1),rs.getString(2), rs.getString(3)));
        }
        rs.close();
        st.close();

    return students;
    }

    public Connection getConnection(){
        return(connection);
    }
    public void close() {
        try {

            connection.close(); }

        catch (SQLException sqle){
            System.err.println("Blad przy zamykaniu polaczenia: " + sqle);

        } }

    public Connection makeConnection(){
        try {
            Class.forName(driver);
            Connection connection = DriverManager.getConnection(url, user, password); return(connection);



        }
        catch(ClassNotFoundException cnfe) {
            System.err.println("Blad ladowania sterownika: " + cnfe);

            return(null);
        }
        catch(SQLException sqle) {
            System.err.println("Blad przy nawiązywaniu polaczenia: " + sqle);

            return(null);
        }
    }
}