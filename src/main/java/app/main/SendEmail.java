package app.main;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class SendEmail {
    private String nadawca;
    private String adressat;
    private String haslo;
    private String wiadomosc;
    private String temat;

    public SendEmail(String adressat,String wiadomosc, String temat) {
        this.adressat = adressat;
        this.wiadomosc = wiadomosc;
        this.temat = temat;

        final String username = "jan.latka.cs@gmail.com";
        final String password = "Haslo1234!";

        Properties prop = new Properties();
        prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.port", "465");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.socketFactory.port", "465");
        prop.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

        Session session = Session.getInstance(prop,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(nadawca));
            message.setRecipients(
                    Message.RecipientType.TO,
                    InternetAddress.parse(adressat)
            );
            message.setSubject(temat);
            message.setText(wiadomosc);

            Transport.send(message);

            System.out.println("Message sent!");

        } catch (MessagingException e) {
            e.printStackTrace();
        }



    }
}
